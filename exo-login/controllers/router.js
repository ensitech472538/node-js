const express = require("express");
const session = require("express-session");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const router = express.Router();
router.use(express.urlencoded({ extended: true }));

router.use(
  session({
    secret: "votre_clé_secrète", // Remplacez par une chaîne de caractères aléatoire pour la sécurité
    resave: false,
    saveUninitialized: true,
  })
);

// Créez un middleware pour vérifier si l'utilisateur est authentifié
const isAuth = (req, res, next) => {
  if (req.session.username) {
    next();
  } else {
    res.redirect("/login");
  }
};

// Créez une route pour afficher la page d'accueil
router.get("/", isAuth, (req, res) => {
  // Ajoutez le middleware isAuth
  res.render("index", { username: req.session.username });
});

router.get("/register", (req, res) => {
  res.render("form-register");
});

router.post("/register", (req, res) => {
  // Accédez aux données du formulaire dans req.body
  const usernameValue = req.body.username;
  const passwordValue = req.body.password;
  const passwordConfirmValue = req.body.passwordConfirm;

  if (passwordValue !== passwordConfirmValue) {
    res.render("form-register", {
      errorMessage: "Les mots de passe ne correspondent pas",
    });
  } else {
    // Stockez la valeur en session
    req.session.username = usernameValue;

    // Hashage du mot de passe
    const salt = bcrypt.genSaltSync(10);
    const hashPassword = bcrypt.hashSync(passwordValue, salt);

    // Stockage du mot de passe hashé
    req.session.hashPassword = hashPassword;

    res.redirect("/login");
  }
});

router.get("/login", (req, res) => {
  res.render("form-login");
});

router.post("/login", (req, res) => {
  // Accédez aux données du formulaire dans req.body
  if (
    req.body.username === req.session.username &&
    bcrypt.compareSync(req.body.password, req.session.hashPassword)
  ) {
    // Stockez la valeur en session
    req.session.username = req.body.username;

    // Création du token
    const token = jwt.sign({ username: req.body.username }, "votre_clé_secrète");

    // Stockage du token
    req.session.token = token;

    res.redirect("/");
  } else {
    res.render("form-login", {
      errorMessage: "Mauvais nom d'utilisateur ou mot de passe",
    });
  }
});

router.get("/logout", (req, res) => {
  // Détruisez la session
  req.session.destroy(() => {
    res.redirect("/");
  });
});

module.exports = router;
