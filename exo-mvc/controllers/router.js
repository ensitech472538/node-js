const express = require("express");
const session = require("express-session");
const router = express.Router();
router.use(express.urlencoded({ extended: true }));

router.use(
  session({
    secret: "votre_clé_secrète", // Remplacez par une chaîne de caractères aléatoire pour la sécurité
    resave: false,
    saveUninitialized: true,
  })
);

// Créez un middleware pour vérifier si l'utilisateur est authentifié
const isAuth = (req, res, next) => {
  if (req.session.username) {
    next();
  } else {
    res.redirect("/user");
  }
};

// Créez une route pour afficher la page d'accueil
router.get("/", isAuth, (req, res) => {
  // Ajoutez le middleware isAuth
  res.render("index", { username: req.session.username });
});

router.get("/user", (req, res) => {
  res.render("form-user");
});

router.post("/user", (req, res) => {
  // Accédez aux données du formulaire dans req.body
  const usernameValue = req.body.username;
  console.log(`Nom d'utilisateur : ${usernameValue}`);

  // Stockez la valeur en session
  req.session.username = usernameValue;

  res.redirect("/");
});

router.get("/logout", (req, res) => {
  // Détruisez la session
  req.session.destroy(() => {
    res.redirect("/");
  });
});

module.exports = router;
