const express = require("express");
const path = require("path");
const routes = require("./controllers/router");
const app = express();

app.set("view engine", "ejs");
app.set("views", path.join(__dirname, "views"));

app.use("/", routes);

app.listen(3000, () => {
  console.log("Server started on port 3000");
});
